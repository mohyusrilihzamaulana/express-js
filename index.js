const express = require('express')
const app = express()
const port = 3000

const handler = (request, response) => {
    response.send('Hi!');
}
app.use((request, response, next) => {
    console.log('1st Middleware');
    next();
})
app.use((request, response, next) => {
    console.log('2st Middleware');
    next();
})
 
app.get('/', handler);

app.get('/user/:name', (request, response) => {
    console.log(request.params);
    response.send(request.params)
 });

app.listen(port, () => {
    console.log(`Server app listening on port ${port}`)
})